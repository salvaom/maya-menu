name = 'maya_menu'
version = '1.0.0'

requires = ['maya', 'six', 'python-3']

build_command = 'python {root}/rezbuild.py {install}'


def commands():
    global env

    env.PYTHONPATH.append('{this.root}/python')
    env.PYTHONPATH .append('{this.root}/resources/maya_init')
