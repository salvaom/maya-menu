import maya.cmds as cmds


def cleanup_unknown_nodes():
    cmds.delete(cmds.ls(type='unknown'))


def sample(**kwargs):
    print kwargs
