try:
    import maya.cmds as cmds
    import maya_modules
    cmds.evalDeferred(maya_modules.init)
except Exception:
    import traceback
    traceback.print_exc()
