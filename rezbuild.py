import sys
import os
import shutil


def build(source_path, build_path, install_path, targets):

    copy_lut = {'source/maya_modules': 'python/maya_modules',
                'resources': 'resources'}

    for src, dst in copy_lut.items():
        src = os.path.join(source_path, src)
        dst = os.path.join(build_path, dst)

        isfile = os.path.isfile(src)

        dirname = os.path.dirname(dst)
        if not os.path.isdir(dirname):
            os.makedirs(dirname)

        if not isfile:
            if os.path.isdir(dst):
                shutil.rmtree(dst)
            shutil.copytree(src, dst)

    if 'install' in (targets or []):
        for folder in os.listdir(build_path):
            src = os.path.join(build_path, folder)
            dst = os.path.join(install_path, folder)

            if not os.path.isdir(src):
                continue

            if os.path.isdir(dst):
                shutil.rmtree(dst)

            if os.path.isdir(dst):
                shutil.rmtree(dst)

            shutil.copytree(src, dst, ignore=shutil.ignore_patterns(
                '*.pyc',
                '.git*')
            )


if __name__ == '__main__':

    build(
        source_path=os.environ['REZ_BUILD_SOURCE_PATH'],
        build_path=os.environ['REZ_BUILD_PATH'],
        install_path=os.environ['REZ_BUILD_INSTALL_PATH'],
        targets=sys.argv[1:]
    )
