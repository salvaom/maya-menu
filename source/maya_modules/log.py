import logging
import maya.utils


def init_logger(level=None, logger_format=None):
    level = level or logging.WARNING
    default_log_format = '%(name)-18s:  %(message)s'
    logger_format = logger_format or default_log_format

    logger = logging.getLogger('maya_modules')
    logger.setLevel(level)
    logger.propagate = False

    handler = maya.utils.MayaGuiLogHandler()

    formatter = logging.Formatter(logger_format)
    handler.setFormatter(formatter)

    logger.addHandler(handler)
