import os
import json
import logging
from maya_modules import definitions
from maya_modules.ui import widgets
import maya.cmds as cmds
import maya.mel as mel
reload(definitions)
reload(widgets)


logger = logging.getLogger(__name__)


class DefinitionManager(object):
    def __init__(self, search_paths=None):
        default = os.getenv('MM_DEFINITIONS_PATH', '').split(os.pathsep)
        self.search_paths = search_paths or default
        self.definitions = []
        self.entry_points = {}
        self.menus = {}
        self.submenus = {}

    def read(self):
        for path in self.search_paths:
            if not os.path.isdir(path):
                logger.debug('Search path "%s" is not a directory' % path)
                continue

            for jsonfile in os.listdir(path):
                jsonfile = os.path.join(path, jsonfile)

                if not os.path.isfile(jsonfile):
                    continue

                if not jsonfile.endswith('.json'):
                    continue

                try:
                    j = open(jsonfile)
                    definition = json.load(j)
                except Exception as e:
                    logger.warning(
                        'Json file "%s" could not be read: %s' % (
                            jsonfile, e)
                    )
                    continue

                finally:
                    j.close()

                try:
                    definition = definitions.Definition(
                        _id=definition['id'],
                        menu=definition['menu'],
                        entry_points=definition['entry_points'],
                        path=jsonfile,
                        requires=definition.get('requires'),
                        variables=definition.get('variables'),
                        environment=definition.get('environment'),

                    )
                    self.definitions.append(definition)
                except Exception as e:
                    logger.warning(
                        'Unable to load definition "%s": %s' % (
                            jsonfile, e
                        )
                    )

    def build(self):
        if cmds.about(batch=True):
            return

        self.entry_points.clear()

        for definition in self.definitions:
            for key, val in definition.entry_points.items():
                if not val.meets_condition():
                    continue

                if key in self.entry_points:
                    logger.warn('Entry point "%s" is duplicated.' % key)

                self.entry_points[key] = val

        menus = list(set([x.menu for x in self.entry_points.values()]))
        for menu in menus:
            self.create_menu(menu)

        ordered_entry_points = sorted(
            self.entry_points.values(),
            key=lambda x: x.get_full_context()
        )

        for ep in ordered_entry_points:
            context, label = ep.get_full_context().split('//')
            ep.reserve_optvars()
            if ep.autorun:
                ep.definition.load()
                try:
                    ep.execute()
                except Exception as e:
                    logger.error(
                        'Failed to execute entry_point "%s": %s' % (
                            ep.id, e
                        )
                    )
            if ep.hidden:
                continue

            submenu = self.create_submenu(context, parent=ep.menu)
            menu = cmds.menuItem(
                ep.id,
                parent=submenu,
                label=label,
                command='maya_modules.manager.execute("%s")' % ep.id,
                i=ep.icon or ''
            )

            if ep.optvars:
                cmds.menuItem(
                    ob=True,
                    c='maya_modules.manager.execute("%s", True)' % ep.id,
                    parent=submenu
                )

        for menu in self.menus.values():
            cmds.menuItem(parent=menu, divider=True)
            cmds.menuItem(
                'reload_%s' % menu,
                parent=menu,
                label='Reload',
                command=self.rebuild
            )

    def rebuild(self, *args):
        for menu in self.menus.values():
            if cmds.menu(menu, exists=True):
                cmds.deleteUI(menu)

        self.definitions = []
        self.menus = {}
        self.submenus = {}

        self.read()
        self.build()

    def create_menu(self, menu):

        if menu in self.menus:
            menu_id = self.menus[menu]
            if cmds.menu(menu_id, exists=True):
                cmds.deleteUI(menu_id)

        if cmds.menu(menu, exists=True):
            cmds.deleteUI(menu)

        gMainWindow = mel.eval('$temp1=$gMainWindow')
        menu_id = cmds.menu(
            menu,
            parent=gMainWindow,
            tearOff=True,
            label=menu
        )
        self.menus[menu] = menu_id

    def create_submenu(self, submenu, parent):
        splitted_menu = submenu.split('.')

        for i in range(len(splitted_menu)):
            submenu = splitted_menu[:i + 1]
            submenu_id = '.'.join(submenu)

            if submenu_id not in self.submenus:
                if i == 0:
                    _parent = self.menus[parent]
                else:
                    _parent = self.submenus['.'.join(submenu[:i])]

                label = submenu[i]
                maya_submenu_id = cmds.menuItem(
                    label,
                    parent=_parent,
                    tearOff=True,
                    label=label,
                    subMenu=True,
                )
                self.submenus[submenu_id] = maya_submenu_id

        return self.submenus[submenu_id]

    def get_entry_point(self, entry_point):
        ep = self.entry_points.get(entry_point)
        if not ep:
            raise KeyError('Entry point "%s" does not exist' % entry_point)
        return ep

    def get_definition(self, definition):
        def_ = None
        for _definition in self.definitions:
            if definition == _definition.id:
                def_ = _definition
                break
        if not def_:
            raise KeyError('Definition "%s" does not exist' % definition)

        return def_

    def execute(self, entry_point, options=False):
        ep = self.get_entry_point(entry_point)

        self.load(ep.definition.id)
        try:
            if options:
                self.show_opt_window(ep)
            else:
                ep.execute()
        except:
            import traceback
            print traceback.format_exc()

    def load(self, definition):
        def_ = self.get_definition(definition)

        for requ in def_.requirements:
            req_def = self.get_definition(requ)
            req_def.load()

        def_.load()

    def show_opt_window(self, ep):
        widgets.OptDialog(ep)
