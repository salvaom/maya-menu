# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'optdialog.ui'
#
# Created: Wed May 04 22:19:59 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCore, QtGui, QtWidgets


class Ui_opt_dialog(object):
    def setupUi(self, opt_dialog):
        opt_dialog.setObjectName("opt_dialog")
        opt_dialog.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(opt_dialog)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setContentsMargins(5, 0, 5, -1)
        self.verticalLayout.setObjectName("verticalLayout")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setObjectName("widget")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.scrollArea = QtWidgets.QScrollArea(self.widget)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 788, 537))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(
            self.scrollAreaWidgetContents)
        self.verticalLayout_2.setContentsMargins(20, -1, 20, -1)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.optvar_layout = QtWidgets.QGridLayout()
        self.optvar_layout.setObjectName("optvar_layout")
        self.verticalLayout_2.addLayout(self.optvar_layout)
        spacerItem = QtWidgets.QSpacerItem(
            20, 508, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.horizontalLayout_2.addWidget(self.scrollArea)
        self.verticalLayout.addWidget(self.widget)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btn_apply = QtWidgets.QPushButton(self.centralwidget)
        self.btn_apply.setObjectName("btn_apply")
        self.horizontalLayout.addWidget(self.btn_apply)
        self.btn_apply_close = QtWidgets.QPushButton(self.centralwidget)
        self.btn_apply_close.setObjectName("btn_apply_close")
        self.horizontalLayout.addWidget(self.btn_apply_close)
        self.btn_close = QtWidgets.QPushButton(self.centralwidget)
        self.btn_close.setObjectName("btn_close")
        self.horizontalLayout.addWidget(self.btn_close)
        self.verticalLayout.addLayout(self.horizontalLayout)
        opt_dialog.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(opt_dialog)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        self.menuEdit = QtWidgets.QMenu(self.menubar)
        self.menuEdit.setObjectName("menuEdit")
        self.menuPresets = QtWidgets.QMenu(self.menuEdit)
        self.menuPresets.setObjectName("menuPresets")
        opt_dialog.setMenuBar(self.menubar)
        self.ac_save = QtWidgets.QAction(opt_dialog)
        self.ac_save.setObjectName("ac_save")
        self.ac_reset = QtWidgets.QAction(opt_dialog)
        self.ac_reset.setObjectName("ac_reset")
        self.actionReset_all_presets = QtWidgets.QAction(opt_dialog)
        self.actionReset_all_presets.setObjectName("actionReset_all_presets")
        self.menuPresets.addSeparator()
        self.menuPresets.addAction(self.actionReset_all_presets)
        self.menuEdit.addAction(self.ac_save)
        self.menuEdit.addAction(self.ac_reset)
        self.menuEdit.addSeparator()
        self.menuEdit.addAction(self.menuPresets.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())

        self.retranslateUi(opt_dialog)
        QtCore.QMetaObject.connectSlotsByName(opt_dialog)

    def retranslateUi(self, opt_dialog):
        opt_dialog.setWindowTitle("Options for:")
        self.btn_apply.setText("Apply")
        self.btn_apply_close.setText("Apply and close")
        self.btn_close.setText("Close")
        self.menuEdit.setTitle("Edit")
        self.menuPresets.setTitle("Presets")
        self.ac_save.setText("Save Settings")
        self.ac_reset.setText("Reset Settings")
        self.actionReset_all_presets.setText("Reset all presets")


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    opt_dialog = QtWidgets.QMainWindow()
    ui = Ui_opt_dialog()
    ui.setupUi(opt_dialog)
    opt_dialog.show()
    sys.exit(app.exec_())
