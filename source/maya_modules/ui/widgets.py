from Qt import QtGui, QtWidgets, QtCore
from maya.app.general.mayaMixin import MayaQWidgetBaseMixin
import maya.cmds as cmds
from . import optdialog
import importlib
importlib.reload(optdialog)


class OptDialog(
        MayaQWidgetBaseMixin, QtWidgets.QMainWindow, optdialog.Ui_opt_dialog):
    def __init__(self, entry_point, parent=None):
        super(OptDialog, self).__init__(parent=parent)
        self.setupUi(self)
        self.entry_point = entry_point

        if self.entry_point.help:
            self.add_help()

        self.optvars = {
            'int': {},
            'float': {},
            'string': {},
            'file': {},
            'bool': {},
            'combo': {},
        }
        self.groups = {}

        self.setWindowTitle('Options for: %s' % self.entry_point.label)

        self.add_widgets()

        self.btn_close.clicked.connect(self.close)
        self.ac_save.triggered.connect(self.save_optvars)
        self.ac_reset.triggered.connect(self.reset_optvars)
        self.btn_apply_close.clicked.connect(self.on_apply_close)
        self.btn_apply.clicked.connect(self.on_apply)
        self.show()

    def add_help(self):
        self.menu_bar = QtWidgets.QMenuBar()
        self.verticalLayout.setMenuBar(self.menu_bar)
        self.menu = QtWidgets.QMenu('Help')
        self.help_action = QtWidgets.QAction('Show help')
        self.help_action.triggered.connect(self.on_show_help)
        self.menu.addMenu(self.menu)
        self.menu.addAction(self.help_action)

    def add_widgets(self):
        optvars = self.entry_point.get_optvars()
        if not optvars:
            self.verticalLayout_2.insertWidget(0, QtWidgets.QLabel('No optvars.'))

        for optvar in sorted(optvars, key=lambda x: x.index):
            if optvar.type == 'int':
                widget = QtWidgets.QSpinBox(self)
                widget.setMaximum(optvar.max)
                widget.setMinimum(optvar.min)
                widget.setValue(optvar.get())
                widget.setSingleStep(optvar.step)

                self.add_opt_widget(optvar, widget, widget.value)

            if optvar.type == 'float':
                widget = QtWidgets.QDoubleSpinBox(self)
                widget.setMaximum(optvar.max)
                widget.setMinimum(optvar.min)
                widget.setValue(optvar.get())
                widget.setSingleStep(optvar.step)

                self.add_opt_widget(optvar, widget, widget.value)

            if optvar.type == 'string':
                widget = QtWidgets.QLineEdit(self)
                widget.setText(optvar.get())

                self.add_opt_widget(optvar, widget, widget.text)

            if optvar.type == 'file':
                widget = FileBrowserEdit(optvar, self)
                widget.line_edit.setText(optvar.get())

                self.add_opt_widget(optvar, widget, widget.line_edit.text)

            if optvar.type == 'bool':
                widget = QtWidgets.QCheckBox(self)
                widget.setChecked(optvar.get())

                self.add_opt_widget(optvar, widget, widget.isChecked)

            if optvar.type == 'combo':
                widget = QtWidgets.QComboBox(self)
                for item in optvar.items:
                    widget.addItem(item)

                widget.setCurrentIndex(optvar.get())

                self.add_opt_widget(optvar, widget, widget.currentIndex)

    def add_opt_widget(self, optvar, widget, optvar_func):
        label = QtWidgets.QLabel(optvar.label + ':', parent=self)
        if optvar.group:
            group = self.get_group(optvar.group)
            index = group.layout().rowCount()
            group.layout().addWidget(label, index, 0, QtCore.Qt.AlignRight)
            group.layout().addWidget(widget, index, 1)
        else:
            index = self.optvar_layout.rowCount()
            self.optvar_layout.addWidget(label, index, 0, QtCore.Qt.AlignRight)
            self.optvar_layout.addWidget(widget, index, 1)

        self.optvars[optvar.type][optvar.optid] = {
            'func': optvar_func,
            'optvar': optvar
        }

    def get_group(self, group):
        if group in self.groups:
            return self.groups[group]

        widget = QtWidgets.QGroupBox(group)
        widget.setLayout(QtWidgets.QGridLayout())
        self.verticalLayout_2.insertWidget(0, widget)

        self.groups[group] = widget
        return self.groups[group]

    def save_optvars(self):
        for _type, optvars in list(self.optvars.items()):
            for _id, optvar_dict in list(optvars.items()):
                optvar_dict['optvar'].set(optvar_dict['func']())

    def reset_optvars(self):
        for _type, optvars in list(self.optvars.items()):
            for _id, optvar_dict in list(optvars.items()):
                optvar = optvar_dict['optvar']
                optvar.set(optvar.default)

        self.close()

    def on_show_help(self):
        self.entry_point.show_help()

    def on_apply(self):
        self.save_optvars()
        self.entry_point.execute()

    def on_apply_close(self):
        self.save_optvars()
        self.entry_point.execute()
        self.close()


class FileBrowserEdit(QtWidgets.QWidget):
    def __init__(self, optvar, parent=None):
        QtWidgets.QWidget.__init__(self, parent=parent)
        self.optvar = optvar
        self.layout = QtWidgets.QHBoxLayout(self)
        self.layout.setContentsMargins(0, 9, 0, 9)
        self.line_edit = QtWidgets.QLineEdit(self)
        self.browse_btn = QtWidgets.QPushButton('Browse', self)

        self.layout.addWidget(self.line_edit)
        self.layout.addWidget(self.browse_btn)

        self.browse_btn.clicked.connect(self.on_browse)

    def on_browse(self):
        _filter = ['*.%s' % x for x in self.optvar.extensions]
        _filter = ' '.join(_filter)
        _filter = 'Any (%s)' % _filter
        result = cmds.fileDialog2(
            fm=self.optvar.mode,
            fileFilter=_filter
        )

        if result:
            self.line_edit.setText(result[0])
