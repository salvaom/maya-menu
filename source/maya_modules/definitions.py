import os
import logging
import pymel.core as pm
import sys
from pprint import pformat


logger = logging.getLogger(__name__)


class Definition(object):
    def __init__(
            self, _id, menu, entry_points, path, requires=None, variables=None,
            environment=None):
        self.id = _id
        self.menu = menu
        self.path = path
        self.requirements = requires or []
        self.variables = self.expand_dict(variables or {}, split=False)
        self.entry_points = {}
        self.environment = self.expand_dict(environment or {})
        entry_points = entry_points or []

        for entry_point in entry_points:
            try:
                self.add_entry_point(entry_point)
            except (ValueError, KeyError) as e:
                logger.warning(
                    'Failed to load an entry_point in file %s: %s' % (
                        self.path,
                        e
                    )
                )

    def __repr__(self):
        return '<%s.%s "%s">' % (
            __name__,
            self.__class__.__name__,
            self.id
        )

    @staticmethod
    def expand_dict(variables, split=True):
        for key, val in variables.items():

            if isinstance(val, (str, unicode)):
                if split:
                    val = val.split(os.pathsep)
                else:
                    val = [val]
            val = map(os.path.expanduser, val)
            val = map(os.path.expandvars, val)
            variables[key] = val if split else val[0]
        return variables

    def add_entry_point(self, entry_point):
        ep = EntryPoint(
            _id=entry_point['id'],
            label=entry_point['label'],
            context=entry_point['context'],
            command=entry_point['command'],
            definition=self,
            menu=entry_point.get('menu'),
            _help=entry_point.get('help'),
            source_type=entry_point.get('source_type'),
            autorun=entry_point.get('autorun', False),
            hidden=entry_point.get('hidden', False),
            optvars=entry_point.get('optvars'),
            icon=entry_point.get('icon'),
            prefix=entry_point.get('prefix'),
            token=entry_point.get('token'),
            condition=entry_point.get('condition')
        )
        self.entry_points[ep.id] = ep

    def load(self):
        for key, val in self.environment.items():
            for item in val:
                if not item:
                    continue

                item = item.format(**self.variables)

                if key == 'PYTHONPATH' and item not in sys.path:
                    sys.path.append(item)

                if item not in os.getenv(key, ''):
                    if not os.getenv(key):
                        os.environ[key] = item
                    else:
                        os.environ[key] += os.pathsep + item


class EntryPoint(object):
    def __init__(
            self, _id, label, context, command, definition, menu=None,
            _help=None, source_type=None, autorun=False, hidden=False,
            optvars=None, icon='', prefix=None, token=None, condition=None):
        self.id = _id
        self.label = label
        self.definition = definition

        if isinstance(context, list):
            self.context = context

        elif isinstance(context, (str, unicode)):
            self.context = context.split('/')
        else:
            raise ValueError('Context must be a string or a list.')

        self.command = command
        self.menu = menu or self.definition.menu
        self.help = _help
        self.source_type = source_type or 'python'
        self.autorun = autorun
        self.hidden = hidden
        self._optvars = optvars or {}
        self.optvars = []
        self._create_optvars()
        icon = icon or ''
        self.icon = os.path.expandvars(icon).format(**definition.variables)
        self.prefix = prefix
        self.condition = condition

    def get_full_context(self):
        if self.prefix:
            prefix = '%s - ' % self.prefix
        else:
            prefix = ''
        label = prefix + self.label
        context = '.'.join(self.context)
        context += '//' + label
        return context

    def execute(self, options=False):
        kwargs = {}
        command = self.command

        if self.optvars:
            kwargs = map(
                lambda x: pm.mel.eval('optionVar -q "%s"' % x.optid),
                self.optvars
            )
            kwargs = zip([x.id for x in self.optvars], kwargs)
            kwargs = self.format_kwargs(kwargs)
            command += kwargs

        logger.info('Executing command: %s' % command)

        try:
            if self.source_type == 'python':
                pm.python(command)
            else:
                pm.mel.eval(command)
        except Exception as e:
            import traceback
            logger.error(traceback.format_exc())
            logger.error(e)

    def format_kwargs(self, kwargs):
        if self.source_type == 'mel':
            kwargs = ['-%s %s' % (x, y) for (x, y) in kwargs]
            kwargs = ' '.join(kwargs)
            kwargs = ' %s' % kwargs
            kwargs += ';'
        else:
            kwargs = ['%s=%s' % (x, pformat(y)) for (x, y) in kwargs]
            kwargs = ', '.join(kwargs)
            kwargs = '(%s)' % kwargs
        return kwargs

    def _create_optvars(self):
        _map = {
            'int': IntOptVar,
            'float': FloatOptVar,
            'string': StringOptVar,
            'file': FileOptVar,
            'bool': BoolOptVar,
            'combo': ComboOptVar,
        }
        for _id, val in self._optvars.items():
            val['id'] = _id
            obj = _map[val['type']]
            self.optvars.append(obj(self, val))

    def get_optvars(self):
        return self.optvars

    def reserve_optvars(self):
        for optvar in self.optvars:
            optvar.reserve()

    def meets_condition(self):
        if not self.condition:
            return True

        try:
            pm.python(self.condition)
            return True
        except Exception as e:
            logger.debug('Tool %s does not meet condition: %s' % (self.id, e))
            return False


class OptionVar(object):
    def __init__(self, entry_point, data):
        self.entry_point = entry_point
        self._data = data
        self.id = data['id']
        self.optid = str('mm_%s_%s' % (self.entry_point.id, self.id))
        self.type = data['type']
        self.label = data.get('label', self.id)
        self.index = data.get('index', 0)
        self.group = data.get('group', '')

    def reserve(self):
        if not pm.optionVar(exists=self.optid):
            self.set(self.default)

    def set(self, value):
        if self.type in ['int', 'bool', 'combo']:
            args = 'iv'
        elif self.type in ['string', 'file']:
            args = 'sv'
            value = str(value)

        elif self.type in ['float']:
            args = 'fv'

        pm.optionVar(**{args: (self.optid, value)})

    def get(self):
        return pm.optionVar(q=self.optid)


class IntOptVar(OptionVar):
    def __init__(self, entry_point, data):
        super(IntOptVar, self).__init__(entry_point, data)
        self.default = data.get('default', 1)
        self.max = data.get('max', 9999999)
        self.min = data.get('min', -9999999)
        self.step = data.get('step', 1)


class FloatOptVar(IntOptVar):
    def __init__(self, entry_point, data):
        super(FloatOptVar, self).__init__(entry_point, data)


class BoolOptVar(OptionVar):
    def __init__(self, entry_point, data):
        super(BoolOptVar, self).__init__(entry_point, data)
        self.default = data.get('default', False)


class ComboOptVar(OptionVar):
    def __init__(self, entry_point, data):
        super(ComboOptVar, self).__init__(entry_point, data)
        self.default = data.get('default', 0)
        self.items = data.get('items', [])


class StringOptVar(OptionVar):
    def __init__(self, entry_point, data):
        super(StringOptVar, self).__init__(entry_point, data)
        self.default = data.get('default', '')


class FileOptVar(StringOptVar):
    def __init__(self, entry_point, data):
        super(FileOptVar, self).__init__(entry_point, data)
        self.mode = data.get('mode', 0)
        self.extensions = data.get('extensions', [])
