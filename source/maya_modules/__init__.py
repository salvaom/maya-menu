import logging

logging.basicConfig(
    format='%(levelname)-8s - %(name)-18s:  %(message)s',
    level=logging.INFO
)
logger = logging.getLogger('maya_modules')


def init(search_paths=None, logging_level=None, logger_format=None):
    from .menu import DefinitionManager
    global manager
    # init_logger(logging_level, logger_format)
    manager = DefinitionManager(search_paths)
    manager.read()
    manager.build()
    return manager
